
resource "aws_key_pair" "dev-key" {
  key_name   = "${var.owner}-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6YuG9VaOOcXqsCvoLYtxn91iTUw9EQ3Bz/F4WQD0SlFb9TkEwx1NTmK8lblrJdb5Qtc+CndsJaCRvsyu5k/27Esz9+VuJI5gxWQIr2DKGGFUim2Ysp5ypHzFJqZUfqJPigwHZ2b5scl3ZiNArhzbAIG7TZHsFg3DITSc4/zrB3K8nF/wGLOhoeuYVCDYxVGZKwISZIrvEQAaFAGmNBxPkjsYnKkd2DuXUwE1SPllr7Eu7dibjL2Tw+6UrKQC89a0DjWsbANwXJzoz796hLwm5kLYNCl0HS3iM0XjeckcULYeRtHu5UrY7sMwYiXwlJRouEqNYLTXwFgAswlZHuyF9 kevin@kking-lt3"
}

resource "aws_instance" "apics-node" {
  ami           = "ami-0ad99772"
  instance_type = "m5.large"
  subnet_id = "${var.public-a}"
  key_name = "${aws_key_pair.dev-key.key_name}"
  vpc_security_group_ids = ["${aws_security_group.apics-node.id}"]
  user_data = "${file("configure/userdata.sh")}"

  tags {
    Name = "${var.instance-name}"
    Owner = "${var.owner}"
  }

  provisioner "local-exec" {
    command = <<EOD
cat <<EOF > configure/aws_hosts
# This is a generated file.
[dev]
${aws_instance.apics-node.public_ip}

[dev:vars]
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
EOF
EOD
  }

  # install Java
  provisioner "local-exec" {
    command = "aws ec2 wait instance-status-ok --instance-ids ${aws_instance.apics-node.id} && ansible-playbook -i configure/aws_hosts configure/playbook-install-jdk8.yml"
  }

  # build gateway-props.json file...
  provisioner "file" {
    content = <<EOD
{
   "nodeInstallDir"           : "/opt/oracle/gateway",
   "logicalGateway"           : "${var.logical-gateway}",
   "logicalGatewayId"         : "${var.logical-gateway-id}",
   "gatewayNodeName"          : "${var.gateway-name}",
   "managementServiceUrl"     : "${var.management-service-url}",
   "listenIpAddress"          : "${var.listen-ip-address}",
   "publishAddress"           : "${aws_instance.apics-node.private_ip}",
   "gatewayExecutionMode"     : "${var.gateway-execution-mode}",
   "idcsUrl"                  : "${var.idcs-url}",
   "requestScope"             : "${var.request-scope}",
   "prevInstallCleanupAction" : "clean"
}
EOD
    destination = "/home/ec2-user/local-gateway-props.json"
    connection {
      type = "ssh"
      user = "ec2-user"
    }
  }

  # install apics gateway
  provisioner "local-exec" {
    command = "ansible-playbook -i configure/aws_hosts configure/playbook-install-configure-join-apicsgatewaynode.yml"
  }


}

resource "aws_security_group" "apics-node" {
  name = "apics-node-sg-${var.owner}"
  description = "Allow inbound SSH and HTTP traffic from internet"
  vpc_id = "${var.vpc-id}"

  tags {
    Name = "APICS Node SG"
    Owner = "${var.owner}"
  }
}

# SSH Ingress
resource "aws_security_group_rule" "apics-node-ssh" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.apics-node.id}"
}

# HTTP Ingress
resource "aws_security_group_rule" "apics-node-http" {
  type = "ingress"
  from_port = 80
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.apics-node.id}"
}


# HTTP Ingress
resource "aws_security_group_rule" "apics-node-http-outbound" {
  type = "egress"
  from_port = 80
  to_port = 443
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.apics-node.id}"
}