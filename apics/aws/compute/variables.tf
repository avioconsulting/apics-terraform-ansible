

variable "public-a" {
  description = "The public-a"
  type = "string"
}

variable "public-b" {
  description = "The public-b"
  type = "string"
}

variable "private-a" {
  description = "The private-a"
  type = "string"
}

variable "private-b" {
  description = "The private-b"
  type = "string"
}

variable "owner" {
  description = "The owner"
  type = "string"
}

variable "vpc-id" {
  description = "The vpc-id"
  type = "string"
}

variable "instance-name" {
  description = "The name for the AWS Instance"
  type = "string"
}

variable "logical-gateway" {}
variable "logical-gateway-id" {}
variable "gateway-name" {}
variable "management-service-url" {}
variable "listen-ip-address" {}
variable "gateway-execution-mode" {}
variable "idcs-url" {}
variable "request-scope" {}