output "vpc-id" {
  value = "${module.network.vpc-id}"
}

output "apics-host1" {
  value = "${module.compute.apics-node-host}"
}
