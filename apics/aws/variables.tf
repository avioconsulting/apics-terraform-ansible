variable "region" {
  description = "The AWS region for the resources to be created in"
  type = "string"
}

variable "owner" {
  description = "Owner of the resources"
  type = "string"
}

variable "secret-key" {
  description = "AWS Secret Key"
  type = "string"
}

variable "access-key" {
  description = "AWS Access Key"
  type = "string"
}


variable "logical-gateway" {}
variable "logical-gateway-id" {}
variable "gateway-name" {}
variable "management-service-url" {}
variable "listen-ip-address" {}
variable "gateway-execution-mode" {}
variable "idcs-url" {}
variable "request-scope" {}