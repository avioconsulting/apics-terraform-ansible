## AWS Properties
region = "us-west-2"
owner = "your-email@avioconsulting.com"
secret-key = "SECRET"
access-key = "ACCESS"


## Oracle APICS
logical-gateway = "DEV-Gateway"
logical-gateway-id = "101"
gateway-name = "dev-gateway-aws"
management-service-url = "https://avioinstance-gse00014432.apiplatform.ocp.oraclecloud.com:443"
listen-ip-address = "127.0.0.1"
gateway-execution-mode = "Development"
idcs-url = "https://idcs-640f07a032f043a6ac5c0bb02561845d.identity.oraclecloud.com/oauth2/v1/token"
request-scope = "https://7DA9D0E2587C4E1D859D3DE029316F88.apiplatform.ocp.oraclecloud.com:443.apiplatform"
