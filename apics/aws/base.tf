provider "aws" {
  access_key = "${var.access-key}"
  secret_key = "${var.secret-key}"
  region     = "${var.region}"
}


module "network" {
  source = "./network"
  owner = "${var.owner}"
  vpc-cidr = "192.168.16.0/21"
  vpc-name = "apics-vpc"
}

module "compute" {
  source = "./compute"
  owner = "${var.owner}"
  vpc-id = "${module.network.vpc-id}"
  public-a = "${module.network.public-a}"
  public-b  = "${module.network.public-b}"
  private-a = "${module.network.private-a}"
  private-b = "${module.network.private-b}"
  instance-name = "APICS-Node 1"
  logical-gateway = "${var.logical-gateway}"
  logical-gateway-id = "${var.logical-gateway-id}"
  gateway-name = "${var.gateway-name}"
  management-service-url = "${var.management-service-url}"
  listen-ip-address = "${var.listen-ip-address}"
  gateway-execution-mode = "${var.gateway-execution-mode}"
  idcs-url = "${var.idcs-url}"
  request-scope = "${var.request-scope}"
}

